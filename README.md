Ansible nginx Role for Devuan
==================

This role is a fork from the [original nginx project](https://github.com/nginxinc/ansible-role-nginx).

It installs nginx on your target Devuan host.

**Note:** This role is still in active development. There may be unidentified issues and the role variables may change as development continues.

Requirements
------------

**Ansible**

This role was developed and tested with [maintained](https://docs.ansible.com/ansible/latest/reference_appendices/release_and_maintenance.html#release-status) versions of Ansible. Backwards compatibility is not guaranteed.

Instructions on how to install Ansible can be found in the [Ansible website](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html).

**Molecule**

Molecule is used to test the various functionailities of the role. Instructions on how to install Molecule can be found in the [Molecule website](https://molecule.readthedocs.io/en/latest/installation.html).

Installation
------------

**Git**

Use `git clone https://git.devuan.org/configuration-management/ansible-role-nginx` to pull the latest edge commit of the role from Devuan GitLab.

Platforms
---------

This nginx Ansible role supports all platforms supported by Devuan:

```yaml
Devuan:
  versions:
    - ascii
    - beowulf
```

Role Variables
--------------

This role has multiple variables. The descriptions and defaults for all these variables can be found in the directory **`defaults/main`** in the following files:

-   **[defaults/main/main.yml](./defaults/main/main.yml):** NGINX installation variables
-   **[defaults/main/template.yml](./defaults/main/template.yml):** NGINX configuration templating variables
-   **[defaults/main/upload.yml](./defaults/main/upload.yml):** NGINX configuration/HTML/SSL upload variables

Dependencies
------------

None

Example Playbook
----------------

This is a sample playbook file for deploying the Ansible Galaxy NGINX role in a localhost and installing the open source version of NGINX.

```yaml
---
- hosts: localhost
  become: true
  roles:
    - role: nginxinc.nginx
```

This is a sample playbook file for deploying the Ansible Galaxy NGINX role to a dynamic inventory containing the `nginx` tag.

```yaml
---
- hosts: tag_nginx
  remote_user: root
  roles:
    - role: nginxinc.nginx
```

This is a sample playbook file for deploying the Ansible Galaxy NGINX role in a localhost and installing the open source version of NGINX as a simple web server.

```yaml
---
- hosts: localhost
  become: true
  roles:
    - role: ansible-role-nginx
  vars:
    nginx_http_template_enable: true
    nginx_http_template:
      default:
        template_file: http/default.conf.j2
        conf_file_name: default.conf
        conf_file_location: /etc/nginx/conf.d/
        servers:
          server1:
            listen:
              listen_localhost:
                # ip: 0.0.0.0
                port: 80
            server_name: localhost
            error_page: /usr/share/nginx/html
            autoindex: false
            web_server:
              locations:
                default:
                  location: /
                  html_file_location: /usr/share/nginx/html
                  html_file_name: index.html
                  autoindex: false
              http_demo_conf: false
```

This is a sample playbook file for deploying the Ansible Galaxy NGINX role in a localhost and installing the open source version of NGINX as a reverse proxy.

```yaml
---
- hosts: localhost
  become: true
  roles:
    - role: ansible-role-nginx
  vars:
    nginx_http_template_enable: true
    nginx_http_template:
      default:
        template_file: http/default.conf.j2
        conf_file_name: default.conf
        conf_file_location: /etc/nginx/conf.d/
        servers:
          server1:
            listen:
              listen_localhost:
                # ip: 0.0.0.0
                port: 80
                opts:
                  - default_server
            server_name: localhost
            error_page: /usr/share/nginx/html
            autoindex: false
            reverse_proxy:
              locations:
                frontend:
                  location: /
                  proxy_pass: http://frontend_servers
                backend:
                  location: /backend
                  proxy_pass: http://backend_servers
        upstreams:
          upstream_1:
            name: frontend_servers
            lb_method: least_conn
            zone_name: frontend
            zone_size: 64k
            servers:
              frontend_server_1:
                address: 0.0.0.0
                port: 8081
                weight: 1
          upstream_2:
            name: backend_servers
            lb_method: least_conn
            zone_name: backend
            zone_size: 64k
            servers:
              backend_server_1:
                address: 0.0.0.0
                port: 8082
                weight: 1
      frontend:
        template_file: http/default.conf.j2
        conf_file_name: frontend_default.conf
        conf_file_location: /etc/nginx/conf.d/
        servers:
          server1:
            listen:
              listen_localhost:
                ip: 0.0.0.0
                port: 8081
                ssl: false
                opts: []
            server_name: localhost
            error_page: /usr/share/nginx/html
            autoindex: false
            web_server:
              locations:
                frontend_site:
                  location: /
                  proxy_hide_headers:
                    - X-Powered-By
                  html_file_location: /usr/share/nginx/html
                  html_file_name: index.html
                  autoindex: false
              http_demo_conf: false
      backend:
        template_file: http/default.conf.j2
        conf_file_name: backend_default.conf
        conf_file_location: /etc/nginx/conf.d/
        servers:
          server1:
            listen:
              listen_localhost:
                ip: 0.0.0.0
                port: 8082
                ssl: false
                opts: []
            server_name: localhost
            error_page: /usr/share/nginx/html
            autoindex: false
            web_server:
              locations:
                backend_site:
                  location: /
                  html_file_location: /usr/share/nginx/html
                  html_file_name: index.html
                  autoindex: false
              http_demo_conf: false
```

To run any of the above sample playbooks create a `setup-nginx.yml` file and paste the contents. Executing the Ansible Playbook is then as simple as executing `ansible-playbook setup-nginx.yml`.

Uninstallation
--------------

To uninstall, either:
- Change the value of the `nginx_enable` variable to `false` in the used playbook (hereafter `setup-nginx.yml`), passed on to the `ansible-role-nginx` role, or use the following:
```
---
- hosts: localhost
  become: true
  roles:
    - role: ansible-role-nginx
  vars:
    nginx_enable: false
```
- Pass `--extra-vars "nginx_enable=false"` to `ansible-playbook`, such as in `ansible-playbook --extra-vars="nginx_enable=false" setup-nginx.yml`

Cf. ["Variable precedence" in Ansible documentation](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable)

License
-------

[Apache License, Version 2.0](LICENSE)

Author Information
------------------

[Bernard Rosset](https://git.devuan.org/Beer)

Original module:

[Alessandro Fael Garcia](https://github.com/alessfg)

[Grzegorz Dzien](https://github.com/gdzien)

&copy; [NGINX, Inc.](https://www.nginx.com/) 2018 - 2020
